<?php

namespace App\Controller\Api;

use App\Entity\Game;
use App\Services\BalloonService;
use App\Services\CarService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_car')]
class CarController extends AbstractController
{
    #[Route('/car', name: 'api_car')]
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/Api/CarController.php',
        ]);
    }

    #[Route('/save_cars', name: 'save_cars', methods: 'POST')]
    public function addCarsInfo(Request $request, CarService $car){
        $error = 0;
        $message = "";
        $carsInfo = json_decode($request->getContent(), true);
        $user = $this->getUser();
        $result = $car->saveCars($carsInfo, $user);
        /** @var Game $result */
        if($result->getBalloons()->first()){
            $path = '/game/profile';
        }else{
            $path = '/game/balloons';
        }

        return $this->json([
            'message' => $message,
            'path_next' => $path,
            'error' => $error,
        ]);

    }
}
