<?php

namespace App\Entity;

use App\Repository\SurveyQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SurveyQuestionRepository::class)]
class SurveyQuestion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $labelQuestion;

    #[ORM\Column(type: 'datetime_immutable')]
    private $created_at;

    #[ORM\Column(type: 'datetime_immutable')]
    private $updatedAt;

    #[ORM\OneToMany(mappedBy: 'SurveyQuestion', targetEntity: SurveyQuestionDetail::class)]
    private $options;

    #[ORM\OneToMany(mappedBy: 'SurveyQuestion', targetEntity: SurveyAnswer::class)]
    private $surveyAnswers;

    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->surveyAnswers = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->labelQuestion;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->labelQuestion;
    }

    public function setQuestion(string $labelQuestion): self
    {
        $this->labelQuestion = $labelQuestion;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|SurveyQuestionDetail[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(SurveyQuestionDetail $Option): self
    {
        if (!$this->options->contains($Option)) {
            $this->options[] = $Option;
            $Option->setSurveyQuestion($this);
        }

        return $this;
    }

    public function removeLabelOption(SurveyQuestionDetail $Option): self
    {
        if ($this->options->removeElement($Option)) {
            // set the owning side to null (unless already changed)
            if ($Option->getSurveyQuestion() === $this) {
                $Option->setSurveyQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SurveyAnswer[]
     */
    public function getSurveyAnswers(): Collection
    {
        return $this->surveyAnswers;
    }

    public function addSurveyAnswer(SurveyAnswer $surveyAnswer): self
    {
        if (!$this->surveyAnswers->contains($surveyAnswer)) {
            $this->surveyAnswers[] = $surveyAnswer;
            $surveyAnswer->setSurveyQuestion($this);
        }

        return $this;
    }

    public function removeSurveyAnswer(SurveyAnswer $surveyAnswer): self
    {
        if ($this->surveyAnswers->removeElement($surveyAnswer)) {
            // set the owning side to null (unless already changed)
            if ($surveyAnswer->getSurveyQuestion() === $this) {
                $surveyAnswer->setSurveyQuestion(null);
            }
        }

        return $this;
    }
}
