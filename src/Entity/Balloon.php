<?php

namespace App\Entity;

use App\Repository\BalloonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BalloonRepository::class)]
class Balloon
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $clickTotal;

    #[ORM\Column(type: 'integer')]
    private $clickAvailable;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'balloons')]
    #[ORM\JoinColumn(nullable: false)]
    private $Game;

    #[ORM\OneToMany(mappedBy: 'balloon', targetEntity: BalloonDetail::class)]
    private $balloonDetails;

    public function __construct()
    {
        $this->balloonDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClickTotal(): ?int
    {
        return $this->clickTotal;
    }

    public function setClickTotal(int $clickTotal): self
    {
        $this->clickTotal = $clickTotal;

        return $this;
    }

    public function getClickAvailable(): ?int
    {
        return $this->clickAvailable;
    }

    public function setClickAvailable(int $clickAvailable): self
    {
        $this->clickAvailable = $clickAvailable;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->Game;
    }

    public function setGame(?Game $Game): self
    {
        $this->Game = $Game;

        return $this;
    }

    /**
     * @return Collection|BalloonDetail[]
     */
    public function getBalloonDetails(): Collection
    {
        return $this->balloonDetails;
    }

    public function addBalloonDetail(BalloonDetail $balloonDetail): self
    {
        if (!$this->balloonDetails->contains($balloonDetail)) {
            $this->balloonDetails[] = $balloonDetail;
            $balloonDetail->setBalloon($this);
        }

        return $this;
    }

    public function removeBalloonDetail(BalloonDetail $balloonDetail): self
    {
        if ($this->balloonDetails->removeElement($balloonDetail)) {
            // set the owning side to null (unless already changed)
            if ($balloonDetail->getBalloon() === $this) {
                $balloonDetail->setBalloon(null);
            }
        }

        return $this;
    }
}
