<?php

namespace App\Form;

use App\Entity\SurveyAnswer;
use App\Entity\SurveyQuestion;
use App\Entity\SurveyQuestionDetail;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('SurveyQuestionDetail', EntityType::class,[
                'label' => false,
                'class' => SurveyQuestionDetail::class,
                'multiple' => false,
                'expanded' => true,
                'group_by' => function(SurveyQuestionDetail $option){
                    return $option->getSurveyQuestion()->getQuestion();
                },
                'required' => true,
                'choice_attr'=>[
                    'required' => true
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SurveyAnswer::class
        ]);
    }
}
