<?php

namespace App\Services;

use App\Entity\Game;
use App\Entity\ProfileGame;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class ProfileService
{
    public function __construct(EntityManagerInterface $em, GameService $game)
    {
        $this->em = $em;
        $this->game = $game;
    }

    public function saveProfile($data, User $user){
        $game = $this->game->getGameFromUser($user)['Activo'][0]??[];

        if(count($data)>0 && count($game)>0){
            $game = $this->em->getRepository(Game::class)->findOneBy(['id'=>$game['id_game']]);

            if($game->getProfileGames()->first() === false) {
                $profile = new ProfileGame();
                $profile->setGame($game);
                $profile->setBalloonAverage((float)$data["balloonsAverage"]);
                $profile->setCarsAverage((float)$data["carsAverage"]);
                $profile->setProfileAverage((float)$data["profileAverage"]);
                $profile->setLetterProfile($data["profile"]);
                $this->em->persist($profile);
                $this->em->flush($profile);
                $game = $this->em->getRepository(Game::class)->findOneBy(['id' => $game->getId()]);
            }
        }
        return $game;
    }
}