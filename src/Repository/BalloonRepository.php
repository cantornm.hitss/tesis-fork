<?php

namespace App\Repository;

use App\Entity\Balloon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Balloon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Balloon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Balloon[]    findAll()
 * @method Balloon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BalloonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Balloon::class);
    }

    // /**
    //  * @return Balloon[] Returns an array of Balloon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Balloon
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
