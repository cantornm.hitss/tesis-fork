<?php

namespace App\Repository;

use App\Entity\ProfileGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProfileGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfileGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfileGame[]    findAll()
 * @method ProfileGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProfileGame::class);
    }

    // /**
    //  * @return ProfileGame[] Returns an array of ProfileGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProfileGame
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
