<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220217025703 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE balloon (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, click_total INT NOT NULL, click_available INT NOT NULL, INDEX IDX_643B3B90E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE balloon_detail (id INT AUTO_INCREMENT NOT NULL, balloon_id INT NOT NULL, number_balloon INT NOT NULL, click_total INT NOT NULL, click_available INT NOT NULL, INDEX IDX_34F5F08116EB7BB8 (balloon_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cars (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, click_total INT NOT NULL, click_available INT NOT NULL, date_created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', date_update_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_95C71D14E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, status SMALLINT NOT NULL, date_created DATETIME NOT NULL, date_updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_232B318CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE balloon ADD CONSTRAINT FK_643B3B90E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE balloon_detail ADD CONSTRAINT FK_34F5F08116EB7BB8 FOREIGN KEY (balloon_id) REFERENCES balloon (id)');
        $this->addSql('ALTER TABLE cars ADD CONSTRAINT FK_95C71D14E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE balloon_detail DROP FOREIGN KEY FK_34F5F08116EB7BB8');
        $this->addSql('ALTER TABLE balloon DROP FOREIGN KEY FK_643B3B90E48FD905');
        $this->addSql('ALTER TABLE cars DROP FOREIGN KEY FK_95C71D14E48FD905');
        $this->addSql('DROP TABLE balloon');
        $this->addSql('DROP TABLE balloon_detail');
        $this->addSql('DROP TABLE cars');
        $this->addSql('DROP TABLE game');
        $this->addSql('ALTER TABLE messenger_messages CHANGE body body LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE headers headers LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE queue_name queue_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE `user` CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
