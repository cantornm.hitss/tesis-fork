/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, BrowserRouter as Router, Route, Routes} from 'react-router-dom';
// import '../css/app.css';
import Home from './components/Home';
import IndexBallons from './Components/ballons/IndexBallons';
import Cars from './Components/cars'
import Profile from './Components/profile'
import Simulator from "./Components/simulator";

ReactDOM.render(<BrowserRouter>
    <Routes>
        <Route index path="/game" element={<Home images_info={window.REP_IMAGE_APP_PROPS} />} />
        <Route path="/game/balloons" element={<IndexBallons images_info={window.REP_IMAGE_APP_PROPS}/>} />
        <Route path="/game/cars" element={<Cars images_info={window.REP_IMAGE_APP_PROPS}/>} />
        <Route path="/game/profile" element={<Profile images_info={window.REP_IMAGE_APP_PROPS}/>} />
        <Route path="/game/simulator" element={<Simulator images_info={window.REP_IMAGE_APP_PROPS}/>} />
    </Routes>
</BrowserRouter>, document.getElementById('root_games'));
//ReactDOM.render(<Router><Fragment>aca <Home /></Fragment></Router>, document.getElementById('root_games'));