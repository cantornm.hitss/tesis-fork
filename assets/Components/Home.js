import React, {Component} from 'react';
import {Route, Switch, Redirect, Link, withRouter} from 'react-router-dom';
import axios from "axios";
import LoadingUtils from "./utils/LoadingUtils";

class Home extends Component {
    constructor() {
        super();
        this.state = {
            currentGame: {
                id_game: 0,
                balloonTotal: 0,
                balloonAvailable: 0,
                totalCars: 0,
                availableCars: 0,
                loading: false
            }
        };
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    componentDidMount() {
        this.loading(true);
        axios
            .get(this.props.images_info.configInfo.urlGame, {async: false})
            .then(result => {
                if (result.status == 200 && result.data.error == 0) {
                    let games = result.data.games;
                    if (typeof games.Activo != 'undefined') {
                        let game = games.Activo[0];
                        if (game.totalCars > 0 && game.balloonTotal > 0) {
                            window.location = '/game/profile';
                        }
                        this.setState({
                            currentGame: {
                                id_game: game.id_game,
                                balloonTotal: game.balloonTotal,
                                balloonAvailable: game.balloonAvailable,
                                totalCars: game.totalCars,
                                availableCars: game.availableCars
                            }
                        });
                    } else {
                        axios.post(this.props.images_info.configInfo.urlNewGame)
                            .then(result => {
                                if (result.status == 200 && result.data.error == 0) {
                                    let {games} = result.data;
                                    let currentGame = {...this.state.currentGame, ...{id_game: games}};
                                    console.log("currentGame");
                                    this.setState({
                                        currentGame
                                    });
                                }
                            });
                    }
                }
                this.loading(false);
            })
            .catch(error => {
                this.loading(true);
            });

    }

    render() {
        return (
            <div className="container-fluid gtco-features" id="about">
                <LoadingUtils loadingStatus={this.state.loading}/>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4">
                            <h2> Explora las diferentes opciones de juego que tenemos </h2>
                            <p> Ingresa a cada uno de los juego y una vez iniciado no pares hasta terminar asi podremos
                                tener
                                información relevante de ellos. </p>
                        </div>
                        <div className="col-lg-8">
                            <svg id="bg-services"
                                 width="100%"
                                 viewBox="0 0 1000 800">
                                <defs>
                                    <linearGradient id="PSgrad_02" x1="64.279%" x2="0%" y1="76.604%" y2="0%">
                                        <stop offset="0%" stopColor="rgb(1,230,248)" stopOpacity="1"/>
                                        <stop offset="100%" stopColor="rgb(29,62,222)" stopOpacity="1"/>
                                    </linearGradient>
                                </defs>
                                <path fillRule="evenodd" opacity="0.102" fill="url(#PSgrad_02)"
                                      d="M801.878,3.146 L116.381,128.537 C26.052,145.060 -21.235,229.535 9.856,312.073 L159.806,710.157 C184.515,775.753 264.901,810.334 338.020,792.380 L907.021,652.668 C972.912,636.489 1019.383,573.766 1011.301,510.470 L962.013,124.412 C951.950,45.594 881.254,-11.373 801.878,3.146 Z"/>
                            </svg>
                            <div className="row">
                                <div className="col" disabled={this.state.currentGame.totalCars > 0 ? true : false}>
                                    <div className="card text-center">
                                        <div className="oval"><img className="card-img-top"
                                                                   src={this.props.images_info.itemImages.carros}
                                                                   alt=""/></div>
                                        <div className="card-body">
                                            <h3 className="card-title">Carritos</h3>
                                            <p className="card-text text-dark">Que tan lejos puedes llegar teniendo en
                                                cuenta la velocidad y el consumo de combustible.</p>
                                            <Link to="/game/cars">Entrar <i className="fa fa-angle-right"
                                                                            aria-hidden="true"></i></Link>
                                        </div>
                                    </div>
                                    {/*<div className="card text-center">
                                        <div className="oval"><img className="card-img-top" src="images/marketing.png" alt="" /></div>
                                        <div className="card-body">
                                            <h3 className="card-title">Marketing</h3>
                                            <p className="card-text">Nullam quis libero in lorem accumsan sodales. Namvel nisieget.</p>
                                        </div>
                                    </div>*/}
                                </div>
                                <div className="col" disabled={this.state.currentGame.balloonTotal > 0 ? true : false}>
                                    <div className="card text-center">
                                        <div className="oval"><img className="card-img-top"
                                                                   src={this.props.images_info.itemImages.globo}
                                                                   alt=""/></div>
                                        <div className="card-body">
                                            <h3 className="card-title">Globos</h3>
                                            <p className="card-text text-dark">Ingresa y mide cuanto crees que puedes
                                                hacer crecer el globo.</p>
                                            <Link to="/game/balloons">Entrar <i className="fa fa-angle-right"
                                                                                aria-hidden="true"></i></Link>
                                        </div>
                                    </div>
                                    {/*<div className="card text-center">
                                        <div className="oval"><img className="card-img-top"
                                                                   src="images/graphics-design.png" alt=""/></div>
                                        <div className="card-body">
                                            <h3 className="card-title">Graphics Design</h3>
                                            <p className="card-text">Nullam quis libero in lorem accumsan sodales. Nam
                                                vel nisi
                                                eget.</p>
                                        </div>
                                    </div>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;