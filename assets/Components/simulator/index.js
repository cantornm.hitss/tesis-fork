import React, {Component} from "react";
import {Chart} from "react-google-charts";
import {createTheme} from '@mui/material/styles';
import {
    Alert,
    Box,
    Button, Card, CardMedia, Chip,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Divider,
    Fab,
    FormControl, IconButton,
    InputLabel,
    MenuItem, Modal,
    Select,
    Snackbar, styled,
    Table,
    TableBody,
    TableCell, tableCellClasses,
    TableContainer,
    TableHead,
    TableRow,
    TextField, ThemeProvider, Typography
} from "@mui/material";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import SaveIcon from '@mui/icons-material/Save';
import Paper from '@mui/material/Paper';
import PlayIcon from '@mui/icons-material/PlayArrow';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import InstrumentOption from './InstrumentOption';
import Instruments from './classes/instruments';
import Recommendations from "./recommendations";
import News from './classes/news';
import Glossary from './Glossary';
import {DateTime} from 'luxon';
import {EXPONDIST} from "@formulajs/formulajs";
import {Pause} from "@mui/icons-material";
import axios from "axios";
import LoadingUtils from "../utils/LoadingUtils";
import * as PropTypes from "prop-types";
import InvestmentDataComponent from "./InvestmentDataComponent";
import VideoComponent from "./VideoComponent";

class Index extends Component {
    constructor() {
        super();
        this.instrument = new Instruments();
        this.news = new News();

        let randomNumber = Math.random();
        this.state = {
            speed: 500,
            profile: "",
            user: "",
            userGame: {},
            nameInvestment: "",
            messageAlert: "",
            news: this.news.getFirstNew(),
            quantityInvestment: 0,
            amountInvestment: 0,
            infoInvestment: {},
            availableBalance: 2000000,
            totalInvestment: 2000000,
            pointsAvailable: 0,
            investments: [
                /*{name:"prueba", quantity:"500", purchaseValue:0, currentValue:0,progress:0,performance:0}*/
            ],
            instrumentSelect: [],
            instrumentSelected: [],
            firstInstrument: {},
            portafolio: [],
            openDialog: false,
            openDialogGraphic: false,
            openDialogShopping: false,
            openSnackBar: false,
            openModalExtra: false,
            openGlossary: false,
            openVideoExplain: false,
            modalPortafolio: false,
            accessShopping: 0,
            typeSnack: "error",
            status: true, // Status
            flag: false, //Bandera
            completeForecast: false,
            changeNews: true,
            nextEvent: randomNumber, //Aleatorio_Evento_Proximo
            randomCurrentEvent: randomNumber, //Aleatorio_Evento_Actual
            targetTime: 0, //Target_Time
            currentTime: 0, //Tiempo_Actual
            distExp: 0, //Dist_Exp
            distExpAprox: 0, //Dist_Exp_Aprox
            T_Inversiones_acciones_ME: 0,
            t_inversiones_simulado: 0,
            T_Inversiones_finca_raiz: 0,
            t_inversiones_fr_simulado: 0,
            pause: false,
            infoGraphics: [
                ['Tiempo', 'inversiones'],
                [1, -0.5],
                [2, 0.4],
                [3, 0.5],
                [4, 2.9],
                [5, 6.3],
                [6, 9],
                [7, 10.6],
                [8, 10.3],
                [9, 7.4]
            ],
            impactMatrix: [],
            matrizSelected: {}
        }

        this.handlerDialog = this.handlerDialog.bind(this);
        this.handlerDialogShopping = this.handlerDialogShopping.bind(this);
        this.handlerDialogGraphics = this.handlerDialogGraphics.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.addInvestment = this.addInvestment.bind(this);
        this.showGraphics = this.showGraphics.bind(this);
        this.filterInstrument = this.filterInstrument.bind(this);
        this.pauseSimulation = this.pauseSimulation.bind(this);
        this.increaseSpeed = this.increaseSpeed.bind(this);
        this.closeSnackBar = this.closeSnackBar.bind(this);
        this.sellInvestment = this.sellInvestment.bind(this);
        this.pauseSimulatorNews = this.pauseSimulatorNews.bind(this);
        this.forecasts = this.forecasts.bind(this);
        this.handleModalExtra = this.handleModalExtra.bind(this);
        this.getGame = this.getGame.bind(this);
        this.handlerDialogVideo = this.handlerDialogVideo.bind(this);
    }

    componentDidMount() {
        let self = this;
        this.setState({
            user: this.props.images_info.info.user,
            instrumentSelect: this.instrument.getAllInvestment(),
            instrumentSelected: this.instrument.getNameAllInvestment()
        });

        axios.get(`${this.props.images_info.configInfo.fileDatos}`).then(data => {
            window.localStorage['datos'] = JSON.stringify(data.data);
        }).catch(err => {
            console.log(err);
        });

        this.loadInstrumentData();
        this.loadMatrizData();
        this.loading(true);
        axios
            .get(this.props.images_info.configInfo.urlGame, {async: false})
            .then(result => {
                if (result.status == 200 && result.data.error == 0) {
                    let games = result.data.games;
                    if (typeof games.Activo != 'undefined') {
                        let game = games.Activo[0];
                        let userGame = {
                            id_game: game.id_game,
                            balloonTotal: game.balloonTotal,
                            balloonAvailable: game.balloonAvailable,
                            totalCars: game.totalCars,
                            availableCars: game.availableCars,
                            clickFast: game.clickFast,
                            letterProfile: game.letterProfile
                        }
                        this.setState({
                            userGame
                        });
                        this.calculateAverage();
                    }
                }
                this.loading(false);
                this.getGame();

                // self.handlerDialogVideo(true);

            })
            .catch(error => {
                this.loading(false);
            });

        setInterval(() => {
            this.saveGame()
        }, 120000);
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    calculateAverage() {
        let balloonsAverage = this.state.userGame.balloonTotal / this.state.userGame.balloonAvailable;
        let carsAverage = this.state.userGame.clickFast / this.state.userGame.totalCars;
        let profileAverage = ((carsAverage + balloonsAverage) / 2).toFixed(2);
        let profile = this.state.userGame.letterProfile;
        let profileName = (this.state.userGame.letterProfile == "b") ? "conservador" : (this.state.userGame.letterProfile == "y") ? "moderado" : "arriesgado";
        let userGameTemp = this.state.userGame;
        let userGame = {
            ...userGameTemp, balloonsAverage: balloonsAverage.toFixed(2),
            carsAverage: carsAverage.toFixed(2),
            profileAverage: profileAverage,
            profile,
            profileName
        }
        this.setState({userGame});
    }

    pauseSimulatorNews() {
        console.log('Pausar');
        this.pauseSimulation();
        let matriz = this.state.matrizSelected;

        let self = this;
        let points = this.state.pointsAvailable;
        let availableBalance = this.state.availableBalance;
        let investments = this.state.investments.map((investment, index) => {
            if (investment.status) {
                let infoMatriz = investment.infoComplete.type.instrumentMatriz;
                let valueMatrix = matriz[infoMatriz];

                investment.valuePopper = (valueMatrix == investment.forecast) ? 1 : 0;
                points = (valueMatrix == investment.forecast) ? points + 1 : points;
                investment.openPopper = true;
                eval("self.refs.runClear" + index + ".clearSelectedInfo()");
                delete investment.forecast;
            }
            return investment;
        });

        if (this.state.pointsAvailable != points && (points % 50) == 0) {
            availableBalance += 100000;
            this.handleModalExtra(true);
        }

        this.setState({
            typeSnack: "warning",
            messageAlert: "Se actualizó la noticia",
            openSnackBar: true,
            changeNews: true,
            investments,
            pointsAvailable: points,
            availableBalance
        });
    }

    runSimulator() {
        console.log(this.state.currentTime);
        let investments = this.state.investments.map((investment, index) => {
            investment.openPopper = false;
            return investment;
        });
        this.setState({
            flag: false,
            status: true,
            pause: true,
            changeNews: false,
            investments
        });
        var init = setInterval(() => {
            this.main();
            if (!this.state.status) {
                clearInterval(init);
            } else {
                this.setState({
                    currentTime: (this.state.currentTime + 1),
                    news: this.news.getNews(this.state.currentTime, this.pauseSimulatorNews)
                });
            }
            console.log('simular');
        }, this.state.speed);
    }

    main() {
        this.loadMatrizSelected();
        this.defineEvent();
        // this.stopCriteria();
        this.invertir();
    }

    loadMatrizSelected() {
        this.setState({
            matrizSelected: this.getMatrizRandom()
        });
    }

    invertir() {
        this.calculateInvestment();
        // this.Acciones_ME();
        // this.finca_raiz();
        // this.Aplicar_Variaciones();
    }

    calculateInvestment() {
        let matriz = this.state.matrizSelected;
        let instrument = JSON.parse(localStorage.getItem('instrument'));
        let instrumentBitcoins = JSON.parse(localStorage.getItem('instrumentBitcoins'));
        let investments = this.state.investments;
        investments = investments.map((value, index) => {
            let positionInstrument = this.numeroAleatorio(0, 999);
            let positionInstrumentBitcoin = this.numeroAleatorio(0, 3999);
            let infoMatriz = value.infoComplete.type.instrument;
            let infoMatrizSelected = value.infoComplete.type.instrument;
            let valueMatriz = matriz[infoMatrizSelected];
            let columnName = (parseInt(valueMatriz) > 0) ? ' max' : (parseInt(valueMatriz) < 0) ? ' min' : "";
            let divisas = (value.name == 'Dólar') ? " US" : (value.name == 'Euro') ? " EU" : "";
            let valueInvestment = (!value.status) ? (value.currentValue) : ((value.name.includes('CDT')) ?
                value.purchaseValue :
                (value.name == 'Euro') ?

                    (value.currentValue + parseFloat(instrument[positionInstrument][(infoMatriz + divisas + columnName)])) :
                    (value.currentValue * (1 + parseFloat(
                        (value.infoComplete.type.instrument !== 'Criptomoneda') ?
                            (instrument[positionInstrument][(infoMatriz + divisas + columnName)]) :
                            (instrumentBitcoins[positionInstrumentBitcoin][(infoMatriz)])
                    ))));
            let indicator = ((value.name.includes('CDT')) ?
                0 :
                parseFloat(
                    (value.infoComplete.type.instrument !== 'Criptomoneda') ?
                        (instrument[positionInstrument][(infoMatriz + divisas + columnName)]) :
                        (instrumentBitcoins[positionInstrumentBitcoin][(infoMatriz)])
                ));
            if (value.name.includes('CDT')) {
                let valueInvestmentPercentage = value.infoComplete.referenceValue;
                let dayInvestment = parseInt(value.infoComplete.type.periodicity);
                if (dayInvestment >= value.infoComplete.type.days) {
                    value.infoComplete.referenceValue = valueInvestmentPercentage;
                    value.history.push(valueInvestmentPercentage);
                    value.infoComplete.type.days++;
                } else {
                    if (value.status) {
                        let balance = value.currentValue;
                        let availableBalance = this.state.availableBalance;
                        let cdtdays = 0;
                        switch (value.infoComplete.type.periodicity) {
                            case "30":
                                cdtdays = 0.08333;
                                break;
                            case "45":
                                cdtdays = 0.125;
                                break;
                            case "60":
                                cdtdays = 0.16666;
                                break;
                            case "360":
                                cdtdays = 1;
                                break;

                            default:
                                cdtdays = 1;
                                break;
                        }

                        let step = (1 + (value.infoComplete.referenceValue / 100));
                        // console.log("1", step);
                        step = Math.pow(step, cdtdays);
                        // console.log("2", step);
                        step = step * balance;
                        // console.log("3", step);

                        availableBalance += step;

                        this.setState({
                            availableBalance
                        });
                        value.status = false;
                        valueInvestment = step;
                    }
                }
            } else {
                if (value.status) {
                    value.history.push(valueInvestment);
                }
            }
            // value.infoComplete.currentValue = valueInvestment;
            value.currentValue = valueInvestment;
            value.indicator = indicator;
            return value;
        });
        this.refs.recommendation.calculateRecommendation();
        this.setState({investments});
    }

    defineEvent() { //Definir_Evento
        if (this.state.flag === false) {
            this.findEvent();
            this.findTargetTime();
        }
    }

    findEvent() { //Hallar_Evento
        this.setState({
            nextEvent: Math.random()
        })
    }

    findTargetTime() { //Hallar_Target_Time
        let num = (EXPONDIST(Math.random(), 3, true) * 10);
        let i = this.roundTime(num);
        console.log(i);
        this.setState({
            flag: true,
            targetTime: this.state.currentTime + i,
            distExp: num,
            distExpAprox: i
        })
    }

    roundTime(num) {//Redondear_Tiempo
        return this.RoundTo(num);
    }

    RoundTo(number, roundto = 1) {
        return roundto * Math.round(number / roundto);
    }

    stopCriteria() { //Criterio_Parada
        let currentTime = this.state.currentTime;
        let targetTime = this.state.targetTime;
        if (currentTime >= (targetTime - 1)) {
            this.pauseSimulation();
        }
    }

    pauseSimulation() {
        this.setState({
            flag: false,
            status: false,
            pause: false,
            randomCurrentEvent: this.state.nextEvent
        });
    }

    increaseSpeed() {
        let speed = (this.state.speed < 333.33) ? 1000 : (this.state.speed - 333.34)
        this.setState({
            speed
        })
    }

    numeroAleatorio(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    sellInvestment(event) {
        let id_graphics = event.currentTarget.id;
        let investments = this.state.investments;
        let investment = investments[id_graphics];
        let balance = this.state.availableBalance;
        // let totalInvestment = this.state.totalInvestment;
        // let purchaseValue = investment.currentValue - investment.purchaseValue;

        balance += investment.currentValue;
        // totalInvestment += purchaseValue;
        investment.status = false;
        investments[id_graphics] = investment;

        this.setState({
            availableBalance: balance,
            // totalInvestment,
            investments
        });
        this.refs.recommendation.calculateRecommendation();
    }

    addInvestment() {
        // console.log(this.state.infoInvestment.currentValue);
        // console.log(this.state.amountInvestment, this.state.quantityInvestment);
        let value = ((parseFloat(this.state.amountInvestment) > 0) ? parseFloat(this.state.amountInvestment) : parseFloat(this.state.quantityInvestment));
        // console.log(value);
        let investment = {
            name: this.state.nameInvestment,
            quantity: (this.state.amountInvestment != "") ? this.state.amountInvestment : this.state.quantityInvestment,
            amount: this.state.amountInvestment,
            history: [],
            purchaseValue: (this.state.nameInvestment.includes('CDT')) ? parseFloat(this.state.amountInvestment) : (this.state.infoInvestment.currentValue * value),
            currentValue: (this.state.nameInvestment.includes('CDT')) ? parseFloat(this.state.amountInvestment) : (this.state.infoInvestment.currentValue * value),
            progress: 0,
            performance: 0,
            indicator: 0,
            infoComplete: this.state.infoInvestment,
            openPopper: false,
            valuePopper: 0
        }
        investment.status = true;
        let balance = this.state.availableBalance - investment.purchaseValue;

        if (balance < 0) {
            this.setState({
                typeSnack: "error",
                openSnackBar: true,
                messageAlert: "Excede el saldo disponible"
            });
            return false;
        }

        let investments = this.state.investments;
        investments.push(investment);
        this.setState({
            investments,
            infoInvestment: {},
            nameInvestment: "",
            quantityInvestment: 0,
            amountInvestment: 0,
            priceCalculator: 0,
            typeSnack: "success",
            messageAlert: "Inversión agregada",
            openSnackBar: true,
            availableBalance: balance
        });

        this.handlerDialog(false);
        this.refs.recommendation.calculateRecommendation();
    }

    handlerDialog(value) {
        this.setState({
            openDialog: value,
            infoInvestment: {},
            nameInvestment: "",
            quantityInvestment: 0,
            amountInvestment: 0,
            priceCalculator: 0,
        });
    }

    handlerDialogShopping(value) {
        let modalPortafolio = false;
        let accessShopping = this.state.accessShopping;
        if (this.state.accessShopping <= 0) {
            accessShopping += 1;
            modalPortafolio = true;
        }
        this.setState({
            openDialogShopping: value,
            accessShopping,
            modalPortafolio
        });
    }

    handlerDialogGraphics(value) {
        this.setState({
            openDialogGraphic: value
        });
    }

    handleModalExtra(value) {
        this.setState({
            openModalExtra: value
        });
    }

    handlerModalPortafolio(value) {
        this.setState({
            modalPortafolio: value
        });
    }

    handlerDialogGlossary(value = false) {
        this.setState({
            openGlossary: value
        });
    }

    handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        let object = {};

        if (name == 'nameInvestment') {
            let info = this.instrument.getInstrument(value);
            console.log(info[0]);
            object['infoInvestment'] = info[0];
            object['infoInvestment'].type.expiration = DateTime
                .now()
                .plus({
                        days: (typeof object.infoInvestment.type.periodicity != 'undefined')
                            ? parseInt(object.infoInvestment.type.periodicity)
                            : 0
                    }
                ).toFormat('yyyy/MM/dd');
            object['infoInvestment'].type.purchaseDate = DateTime
                .now()
                .toFormat('yyyy/MM/dd');
            object['priceCalculator'] = 0;
        } else if (name == 'amountInvestment' || name == 'quantityInvestment') {
            object['priceCalculator'] = this.state.infoInvestment.currentValue * value;
        }
        object[name] = value;

        this.setState(object);
    }

    showGraphics(event) {
        let id_graphics = event.currentTarget.id;
        let history = this.state.investments[id_graphics].history;
        history = history.map((value, index) => {
            return [(index + 1), parseFloat(value.toFixed(2))];
        });
        history.unshift(['Tiempo', 'inversiones']);
        this.setState({
            infoGraphics: history
        });

        this.handlerDialogGraphics(true);
    }

    filterInstrument(type) {
        let instrument = this.state.instrumentSelect;
        instrument = instrument.filter((value) => {
            return value.type.type.includes(type)
        });
        let firstInstrument = instrument[0];
        this.setState({
            instrumentSelected: instrument,
            firstInstrument
        });

        this.handlerDialog(true);
    }

    loadInstrumentData() {
        axios.get(`${this.props.images_info.configInfo.fileInstruments}`).then(data => {
            window.localStorage['instrument'] = JSON.stringify(data.data);
        }).catch(err => {
            console.log(err);
        });

        axios.get(`${this.props.images_info.configInfo.fileInstrumentsBitcoins}`).then(data => {
            window.localStorage['instrumentBitcoins'] = JSON.stringify(data.data);
        }).catch(err => {
            console.log(err);
        });

        axios.get(`${this.props.images_info.configInfo.portafolio}`).then(data => {
            this.setState({portafolio: data.data});
        }).catch(err => {
            console.log(err);
        });
    }

    loadMatrizData() {
        axios.get(`${this.props.images_info.configInfo.fileMatriz}`).then(data => {
            this.setState({
                impactMatrix: data.data
            });
        }).catch(err => {
            console.log(err);
        });
    }

    getMatrizRandom() {
        let allMatriz = this.state.impactMatrix;
        let randomNumber = this.numeroAleatorio(0, 13);

        return allMatriz[randomNumber];
    }

    closeSnackBar() {
        this.setState({
            openSnackBar: false
        })
    }

    forecasts(investment, value, id) {
        let completeForecast = true;
        let changeNews = true;
        let investments = this.state.investments;
        investments[id].forecast = value;

        let filterInvestment = investments.filter(investment => {
            if (!investment.status) {
                return false;
            }
            if (typeof investment.forecast != 'undefined') {
                if (investment.forecast != null) {
                    return false
                } else {
                    return true;
                }
            } else {
                return true;
            }
        });

        if (filterInvestment.length <= 0) {
            completeForecast = false;
            changeNews = false;
            this.pauseSimulation();
        }

        this.setState({
            investments,
            completeForecast,
            changeNews
        });
    }

    saveGame() {
        this.loading(true);
        let state = {
            ...this.state, openDialog: false,
            openDialogGraphic: false,
            openDialogShopping: false,
            openSnackBar: false,
            openModalExtra: false,
            openGlossary: false,
            modalPortafolio: false
        };
        let simulador = JSON.stringify(state);
        axios.post(`${this.props.images_info.configInfo.urlSaveGame}`, {simulador, game: this.state.userGame.id_game})
            .then((result) => {
                this.loading(false);
            })
            .catch(error => {
                console.error(error);
                this.loading(false);
            });
    }

    getGame() {
        this.loading(true);
        let simulador = JSON.stringify(this.state);
        let self = this;
        axios.get(`${this.props.images_info.configInfo.urlGetGame}/${this.state.userGame.id_game}`)
            .then((result) => {
                this.setState(JSON.parse(result.data));
                self.news.setWait(this.state.currentTime + self.numeroAleatorio(1, 30));
                this.loading(false);
            })
            .catch(error => {
                console.error(error);
                this.loading(false);
            });
    }


    handlerDialogVideo(value) {
        this.setState({
            openVideoExplain: value
        });
    }

    render() {
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: '60%',
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };

        const StyledTableCell = styled(TableCell)(({theme}) => ({
            [`&.${tableCellClasses.head}`]: {
                backgroundColor: theme.palette.common.black,
                color: theme.palette.common.white,
            },
            [`&.${tableCellClasses.body}`]: {
                fontSize: 14,
            },
        }));

        const StyledTableRow = styled(TableRow)(({theme}) => ({
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.action.hover,
            },
            // hide last border
            '&:last-child td, &:last-child th': {
                border: 0,
            },
        }));

        const theme = createTheme({
            components: {
                MuiIcon: {
                    styleOverrides: {
                        root: {
                            // Match 24px = 3 * 2 + 1.125 * 16
                            boxSizing: 'content-box',
                            padding: 3,
                            fontSize: '1.125rem',
                        },
                    },
                },
            },
        });


        return (
            <React.Fragment>
                <VideoComponent openVideoExplain={this.state.openVideoExplain}
                                videoLink={this.props.images_info.itemVideos.video_explicativo}
                                videoHandler={this.handlerDialogVideo}
                />
                <Dialog
                    fullWidth={true}
                    maxWidth={'lg'}
                    open={this.state.openGlossary}
                    onClose={() => this.handlerDialogGlossary(false)}
                    onLoad={() => this.handlerDialogGlossary(false)}
                >
                    <DialogTitle>Glosario</DialogTitle>
                    <DialogContent>

                        <Glossary/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.handlerDialogGlossary(false)}>Cerrar</Button>
                    </DialogActions>
                </Dialog>

                <div className="container-fluid gtco-features" id="about">
                    <LoadingUtils loadingStatus={this.state.loading}/>

                    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}} open={this.state.openSnackBar}
                              autoHideDuration={6000} onClose={this.closeSnackBar}>
                        <Alert severity={this.state.typeSnack} sx={{width: '100%'}}>
                            {this.state.messageAlert}!
                        </Alert>
                    </Snackbar>
                    <div className={"row"}>
                        <div className="col-md-8">
                            <Fab color="secondary" aria-label="Guardar" onClick={() => this.saveGame()}>
                                <SaveIcon/>
                            </Fab>
                            <Fab variant="extended" color="primary" aria-label="glosario"
                                 onClick={() => this.handlerDialogGlossary(true)}>
                                Glosario
                            </Fab>
                            <Fab variant="extended" color="primary" aria-label="explicacion"
                                 onClick={() => this.handlerDialogVideo(true)}>
                                Explicación
                            </Fab>
                            <div className="row">
                                <div className="col-sm-6">
                                    <h4> {this.state.user} </h4>
                                </div>
                                <div className="col-sm-6">
                                    <h3>Nivel</h3> <br/>
                                    <h4 className={"text-capitalize"}>{(typeof this.state.userGame.profileName != 'undefined') ? (this.state.userGame.profileName) : ("")}</h4>
                                </div>
                            </div>
                            <Divider/>
                            <div className="row table-responsive row d-flex justify-content-center">
                                <table className={"table table-borderless"}>
                                    <tbody>
                                    <tr>
                                        <td scope={"row"} className="text-center">
                                            <h3>Tu dinero disponible</h3> <br/>
                                            <span
                                                className={"h2 text-center text-dark text-decoration-underline"}>
                                            {new Intl.NumberFormat("ES-ES", {
                                                style: "currency",
                                                currency: "COP"
                                            }).format(this.state.availableBalance)}
                                        </span>
                                        </td>
                                        <td className="text-center">
                                            <h3>Puntos ganados</h3> <br/>
                                            <span className={"h2 text-center text-dark text-decoration-underline"}>
                                            {this.state.pointsAvailable}
                                        </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-md-4 text-center d-flex align-items-center">
                            <Container fixed>
                                <div className="row text-center">
                                    <h1>Noticias</h1>
                                </div>
                                <div className="row text-center">
                                    <p className={"text-dark"}>{this.state.news}</p>
                                </div>
                            </Container>
                        </div>
                    </div>
                    <Divider/>
                    <div className="row">
                        <h3>Sección de recomendaciones: </h3><br/>
                        <Recommendations recommendation={this.props.images_info.configInfo.recommendation}
                                         investements={this.state.investments} time={this.state.currentTime}
                                         userGame={this.state.userGame}
                                         totalInvestment={this.state.totalInvestment}
                                         availableBalance={this.state.availableBalance}
                                         news={this.state.news}
                                         ref="recommendation"
                        />
                    </div>
                    <Divider/><br/>
                    <Container maxWidth="xl" sx={{border: 1, borderRadius: 2, boxShadow: 1}}>
                        <div className="row">
                            <div className="col-md-10">
                                <p className={"fw-bold"}>Sus inversiones - <span className={"fs-4 fst-italic"}><strong> Tiempo Actual:</strong> {this.state.currentTime}</span>
                                </p>
                                <Divider/>
                                {(this.state.investments.filter(investment => investment.status).length <= 0) ?
                                    (<div className="row">
                                            No tiene inversiones en este momento
                                        </div>
                                    ) :
                                    (this.state.investments.map((investment, index) => (
                                        (investment.status) ? (
                                            <InvestmentDataComponent key={index} investment={investment}
                                                                     onClick={this.sellInvestment} id={index}
                                                                     onClick1={this.showGraphics}
                                                                     addForecasts={this.forecasts}
                                                                     selectedForecast={this.state.completeForecast}
                                                                     ref={`runClear${index}`}
                                            />) : (
                                            <React.Fragment/>
                                        )
                                    )))
                                }
                            </div>
                            <div className="col-md-2 float-end">
                                <div className="card">
                                    <div className="card-body d-flex justify-content-between align-items-right">
                                        <Box sx={{'& > :not(style)': {m: 1}}}>
                                            {(this.state.pause) ? (
                                                <React.Fragment>
                                                    <Fab color="success" aria-label="pause"
                                                         onClick={() => this.pauseSimulation()}>
                                                        <Pause/>
                                                    </Fab>
                                                    {/*<Fab color="default" aria-label="reset"
                                                 onClick={() => this.increaseSpeed()}>
                                                <DoubleArrowIcon/>
                                            </Fab>*/}
                                                </React.Fragment>
                                            ) : (
                                                <React.Fragment>
                                                    <Fab color="secondary" aria-label="edit"
                                                         disabled={(this.state.changeNews) ? true : false}
                                                         onClick={() => this.runSimulator()}>
                                                        <PlayIcon/>
                                                    </Fab>
                                                    <Fab color="primary" aria-label="add"
                                                         onClick={() => this.handlerDialogShopping(true)}>
                                                        <ShoppingCartIcon/>
                                                    </Fab>
                                                </React.Fragment>
                                            )}

                                        </Box>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                    <Container maxWidth="xl" sx={{border: 1, borderRadius: 2, boxShadow: 1}}>
                        <div className="row">
                            <div className="col-md-10">
                                <p className={"fw-bold"}><strong> Inversiones finalizadas:</strong></p>
                                {(this.state.investments.length <= 0) ?
                                    (<React.Fragment/>
                                    ) :
                                    (this.state.investments.map((investment, index) => (
                                        (!investment.status) ? (
                                            <InvestmentDataComponent key={index} investment={investment}
                                                                     onClick={this.sellInvestment} id={index}
                                                                     onClick1={this.showGraphics}/>) : (
                                            <React.Fragment/>
                                        )
                                    )))
                                }
                            </div>
                        </div>
                    </Container>
                    <Dialog
                        fullWidth={false}
                        maxWidth={'15%'}
                        open={this.state.openDialog}
                    >
                        <DialogTitle>{"Nueva Inversión"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Seleccione la opción de inversión que más le llame la atención.
                            </DialogContentText>
                            <Box
                                noValidate
                                component="form"
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    m: 'auto',
                                    width: 'fit-content',
                                }}
                            >
                                <FormControl sx={{mt: 3, minWidth: 120}}>
                                    <InputLabel htmlFor="max-width">Inversión</InputLabel>
                                    <Select
                                        autoFocus
                                        value={this.state.nameInvestment}
                                        onChange={this.handleChange}
                                        label="maxWidth"
                                        inputProps={{
                                            name: 'nameInvestment',
                                            id: 'nameInvestment',
                                        }}
                                    >
                                        <MenuItem value={false}></MenuItem>
                                        {this.state.instrumentSelected.map((value, index) => (
                                            <MenuItem key={index} value={value.nameType}>{value.nameType}</MenuItem>
                                        ))}
                                    </Select>
                                    {
                                        (typeof this.state.infoInvestment.type != 'undefined') ?
                                            (
                                                typeof this.state.infoInvestment.type.investment != 'undefined' &&
                                                this.state.infoInvestment.type.investment == 'quantity'
                                            ) ?
                                                (
                                                    <TextField
                                                        margin="dense"
                                                        id="quantityInvestment"
                                                        label="Cantidad"
                                                        type="number"
                                                        inputProps={{inputMode: 'numeric', pattern: '[0-9]*', min: '1'}}
                                                        fullWidth
                                                        variant="standard"
                                                        name={"quantityInvestment"}
                                                        onChange={this.handleChange}
                                                    />
                                                ) : (
                                                    <TextField
                                                        margin="dense"
                                                        id="amountInvestment"
                                                        label="Monto"
                                                        type="text"
                                                        fullWidth
                                                        variant="standard"
                                                        name={"amountInvestment"}
                                                        onChange={this.handleChange}
                                                    />
                                                ) : ""
                                    }
                                </FormControl>

                                <TableContainer>
                                    <Table sx={{minWidth: 650}} aria-label="simple table" stickyHeader>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="center" colSpan={3}>
                                                    Información Basica
                                                </TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell>Tipo</TableCell>
                                                {
                                                    (typeof this.state.firstInstrument.nameType != 'undefined') ? (
                                                        (this.state.firstInstrument.nameType.includes("CDT")) ? (
                                                            <React.Fragment>
                                                                <TableCell align="right">Porcentaje</TableCell>
                                                                <TableCell align="right">Fecha Vencimiento</TableCell>
                                                            </React.Fragment>
                                                        ) : (
                                                            <React.Fragment>
                                                                <TableCell align="right">Valor Actual</TableCell>
                                                                <TableCell align="right">Valor Compra</TableCell>
                                                            </React.Fragment>
                                                        )) : (
                                                        <React.Fragment/>
                                                    )
                                                }

                                                {/*<TableCell align="right">Fecha Vencimiento</TableCell>*/}
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow
                                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {(typeof this.state.infoInvestment.type != 'undefined') ? this.state.infoInvestment.type.type : ""}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {(typeof this.state.infoInvestment.currentValue != 'undefined' && typeof this.state.firstInstrument.nameType != 'undefined') ? (
                                                        this.state.firstInstrument.nameType.includes("CDT")) ?
                                                        this.state.infoInvestment.currentValue.toFixed(2) + " %" :
                                                        new Intl.NumberFormat("ES-ES", {
                                                            style: "currency",
                                                            currency: "COP"
                                                        }).format(this.state.infoInvestment.currentValue) : ""
                                                    }
                                                </TableCell>
                                                {(typeof this.state.firstInstrument.nameType != 'undefined') ? (
                                                    !this.state.firstInstrument.nameType.includes("CDT")) ? (
                                                    <TableCell align="right">
                                                        {(typeof this.state.priceCalculator != 'undefined') ? (new Intl.NumberFormat("ES-ES", {
                                                            style: "currency",
                                                            currency: "COP"
                                                        }).format(this.state.priceCalculator)) : ""}
                                                    </TableCell>
                                                ) : (
                                                    <React.Fragment></React.Fragment>
                                                ) : (<React.Fragment></React.Fragment>)}
                                                {(typeof this.state.firstInstrument.nameType != 'undefined') ?
                                                    (this.state.firstInstrument.nameType.includes("CDT")) ?
                                                        (
                                                            <TableCell align="right">
                                                                {(typeof this.state.infoInvestment.type != 'undefined') ? this.state.infoInvestment.type.expiration : ""}
                                                            </TableCell>
                                                        ) : (
                                                            <React.Fragment/>
                                                        ) : (
                                                        <React.Fragment/>
                                                    )}
                                            </TableRow>
                                            <TableRow
                                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                            >
                                                <TableCell align="left">
                                                    {(typeof this.state.infoInvestment.description != 'undefined') ? (
                                                        <details>
                                                            <summary>Más detalles de la inversión</summary>
                                                            <p>{this.state.infoInvestment.description}.</p>
                                                        </details>
                                                    ) : (<React.Fragment/>)}
                                                </TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Box>
                        </DialogContent>
                        <DialogActions>
                            <DialogActions>
                                <Button onClick={() => this.handlerDialog(false)}>Cerrar</Button>
                                {
                                    (
                                        (this.state.nameInvestment != "" && this.state.quantityInvestment > 0) ||
                                        (this.state.nameInvestment != "" && this.state.amountInvestment > 0)
                                    )
                                        ? (<Button onClick={this.addInvestment}>Aceptar</Button>)
                                        : (<div></div>)
                                }
                            </DialogActions>
                        </DialogActions>
                    </Dialog>
                    <Dialog
                        fullScreen
                        open={this.state.openDialogGraphic}
                        // onClose={this.handleClose(false)}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"

                    >
                        <div>
                            <DialogTitle id="alert-dialog-title">
                                {"Grafica de inversiones"}
                            </DialogTitle>
                            <DialogContent>
                                <Box
                                    noValidate
                                    component="form"
                                    sx={{
                                        display: 'flex', flexWrap: 'wrap'
                                    }}
                                >
                                    <Chart
                                        chartType="Line"
                                        data={this.state.infoGraphics}
                                        width="100%"
                                        height="400px"
                                        options={{
                                            chart: {
                                                title: "Inversión en el tiempo",
                                            },
                                            width: 700,
                                            height: 500,
                                            series: {
                                                // Gives each series an axis name that matches the Y-axis below.
                                                0: {axis: "Tiempo"},
                                                1: {axis: "Daylight"},
                                            },
                                            axes: {
                                                // Adds labels to each axis; they don't have to match the axis names.
                                                y: {
                                                    Temps: {label: "Valor"},
                                                    Daylight: {label: "Daylight"},
                                                },
                                            },
                                        }}
                                    />

                                </Box>
                            </DialogContent>
                        </div>
                        <DialogActions>
                            <Button onClick={() => this.handlerDialogGraphics(false)}>Cerrar</Button>
                        </DialogActions>
                    </Dialog>
                    <Dialog
                        fullScreen
                        open={this.state.openDialogShopping}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogActions>
                            <Button onClick={() => this.handlerModalPortafolio(true)}>Ver Recomendaciones</Button>
                            <Button onClick={() => this.handlerDialogShopping(false)}>Finalizar compra</Button>
                        </DialogActions>
                        <div>
                            <DialogTitle id="alert-dialog-title">
                                {"Selecciona el instrumento"}
                            </DialogTitle>
                            <DialogContent>
                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.cdt}
                                                  texto={'CDT'}
                                                  onClick={() => this.filterInstrument('CDT')}/>

                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.divisas}
                                                  texto={'Divisas'}
                                                  onClick={() => this.filterInstrument('Divisas')}/>

                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.criptomonedas}
                                                  texto={'Criptomonedas'}
                                                  onClick={() => this.filterInstrument('Criptomonedas')}/>

                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.bonos}
                                                  texto={'Bonos'}
                                                  onClick={() => this.filterInstrument('Bonos')}/>

                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.acciones_emergentes}
                                                  texto={'Acciones Mercados Emergentes'}
                                                  onClick={() => this.filterInstrument('Emergentes')}/>

                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.acciones}
                                                  texto={'Acciones Mercados Desarrollados'}
                                                  onClick={() => this.filterInstrument('Desarrollados')}/>

                                <InstrumentOption imagesInfo={this.props.images_info.itemImages.ntfs}
                                                  texto={'ETF'}
                                                  onClick={() => this.filterInstrument('ETF')}/>
                            </DialogContent>
                        </div>
                    </Dialog>
                    <Modal
                        open={this.state.modalPortafolio}
                        onClose={() => this.handlerModalPortafolio(false)}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                Recomendaciones por tu perfil
                            </Typography>
                            <Typography id="modal-modal-description" sx={{mt: 2}} component={"div"}>
                                <TableContainer component={Paper}>
                                    <Table sx={{minWidth: 650}} aria-label="simple table">
                                        <caption>Con tú perfil <strong>{this.state.userGame.profileName}</strong> son
                                            recomendables las
                                            anteriores inversiones
                                        </caption>
                                        <TableHead>
                                            <StyledTableRow>
                                                <StyledTableCell>Inversión</StyledTableCell>
                                                <StyledTableCell align="right">Valor</StyledTableCell>
                                                <StyledTableCell align="right">Porcentaje</StyledTableCell>
                                            </StyledTableRow>
                                        </TableHead>
                                        <TableBody key={"table-body-portafolio"}>
                                            {(this.state.portafolio.length > 0) ? (
                                                this.state.portafolio.map((portafolio, index) => (
                                                    (typeof portafolio[this.state.userGame.profileName] != 'undefined') ? (
                                                        (portafolio[this.state.userGame.profileName].INSTRUMENTO != 'N/A') ? (
                                                            <StyledTableRow
                                                                key={`row${index}`}
                                                            >
                                                                <StyledTableCell component="th" scope="row">
                                                                    {portafolio[this.state.userGame.profileName].INSTRUMENTO}
                                                                </StyledTableCell>
                                                                <StyledTableCell
                                                                    align="right">{new Intl.NumberFormat("ES-ES", {
                                                                    style: "currency",
                                                                    currency: "COP"
                                                                }).format(parseFloat(portafolio[this.state.userGame.profileName].VALOR).toFixed(0))}</StyledTableCell>
                                                                <StyledTableCell
                                                                    align="right">{parseFloat(portafolio[this.state.userGame.profileName].PORCENTAJE).toFixed(2)}</StyledTableCell>
                                                            </StyledTableRow>) : (
                                                            <React.Fragment/>
                                                        )
                                                    ) : (<React.Fragment/>)
                                                ))
                                            ) : (
                                                <React.Fragment/>
                                            )}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                <Button onClick={() => this.handlerModalPortafolio(false)}>Cerrar</Button>
                            </Typography>
                        </Box>
                    </Modal>
                    <Modal
                        open={this.state.openModalExtra}
                        onClose={() => this.handleModalExtra(false)}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                Felicitaciones!!!
                            </Typography>
                            <Typography id="modal-modal-description" sx={{mt: 2}}>
                                Felicitaciones por tus buenos pronósticos has conseguido $100.000 pesos para seguir
                                invirtiendo.
                            </Typography>
                        </Box>
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}

export default Index;