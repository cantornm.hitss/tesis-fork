import React, {Component} from "react";
import {Box, Divider, ListItem, ListItemButton, ListItemText, Tab, Tabs, Typography} from "@mui/material";
import {FixedSizeList} from 'react-window';

class Glossary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }

    render() {
        let self = this;

        function handleChange(newValue) {
            let target = newValue.target.id;
            let value = parseInt(target.split('-')[2]);
            self.setState({
                value
            });
        }

        function a11yProps(index = 0) {
            return {
                id: `simple-tab-${index}`,
                'aria-controls': `simple-tabpanel-${index}`,
            };
        }

        function TabPanel(props) {
            const {children, value, index, ...other} = props;

            return (
                <div
                    role="tabpanel"
                    hidden={value !== index}
                    id={`simple-tabpanel-${index}`}
                    aria-labelledby={`simple-tab-${index}`}
                    {...other}
                >
                    {value === index && (
                        <Box sx={{p: 3}}>
                            <Typography>{children}</Typography>
                        </Box>
                    )}
                </div>
            );
        }

        return (
            <div>
                <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                    <Tabs value={this.state.value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="INSTRUMENTOS DE INVERSIÓN" {...a11yProps(0)} />
                        <Tab label="CONCEPTOS" {...a11yProps(1)} />
                        <Tab label="INDICADORES" {...a11yProps(2)}/>
                    </Tabs>
                </Box>
                <TabPanel value={this.state.value} index={0}>
                    <Typography variant="span" component="span">
                        <strong>Acciones: </strong>Son títulos de valor que
                        representan
                        el capital social
                        de una empresa y pueden ser adquiridos por personas naturales o jurídicas, quienes
                        compran
                        un porcentaje de participación en dicha empresa. Además, se obtienen beneficios por la
                        valorización del precio de la acción en las Bolsas de Valores y se conceden derechos
                        económicos y políticos en las asambleas de accionistas, según el tipo de acción que se
                        trate. <samp>Fuente: Banco Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Bonos:</strong> Los bonos son valores que
                        simbolizan
                        deuda de la entidad emisora. Son considerados como una fuente alterna de financiamiento
                        para
                        las empresas, distinta al crédito bancario o la emisión de acciones. La empresa emisora
                        adquirirá la obligación de devolverle el monto de la inversión al tenedor del bono, una
                        vez
                        transcurra un periodo de tiempo, más los intereses que se comprometió a pagar. Teniendo
                        en
                        cuenta que se trata de un valor de deuda, cuando el inversionista compra un bono no
                        asume el
                        rol de socio de la entidad que los emite. Lo anterior debido a que, a diferencia de las
                        acciones, al comprar un bono no se recibirá dividendos y no se podrá participar en la
                        asamblea de accionistas. Los bonos están clasificados como valores de renta fija, ya que
                        el
                        inversionista tiene conocimiento anticipado de cuál será su ganancia al vencimiento de
                        este.
                        El inversionista podrá vender el bono antes del vencimiento, sin embargo, el retorno
                        económico dependerá de las condiciones del mercado. <samp>Fuente: Banco Scotiabank
                            Colpatria.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>CDT:</strong> Un certificado de depósito a
                        término
                        (CDT)
                        es un instrumento de inversión definido mediante certificado, que posibilita la
                        inversión de
                        una cantidad de dinero en una entidad financiera por un plazo determinado, generalmente
                        30,
                        60, 90, 180 o 360 días.<samp> Fuente: Banco de la República de Colombia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Criptomonedas: </strong>Una criptomoneda es un
                        activo
                        digital que usa un cifrado criptográfico que garantiza su titularidad y asegura la
                        integridad de las transacciones, lo cual permite controlar la generación de unidades
                        adicionales, es decir, impedir que alguien pueda realizar copias.<samp> Fuente: Banco
                            Santander.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Divisas:</strong> El mercado Foreign Exchange o
                        FX,
                        de
                        divisas (Monedas extranjeras utilizadas en el comercio internacional) es un mercado
                        fundamentalmente no organizado, denominado en inglés 'Over The Counter' (OTC). Hoy en
                        día es
                        el mercado financiero más grande del mundo.<samp> Fuente: Banco BBVA</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>ETF:</strong> Los fondos cotizados o Exchange
                        Traded
                        Funds (ETF), son fondos de inversión que pretenden replicar un índice, generalmente de
                        acciones, mediante la constitución de un portafolio integrado por algunos o todos los
                        activos que conforman dicho índice. A diferencia de los fondos tradicionales, las
                        unidades
                        de participación de un ETF pueden ser negociadas en el mercado secundario como una
                        acción.
                        <samp>Fuente: Banco Davivienda</samp>
                    </Typography>
                </TabPanel>
                <TabPanel value={this.state.value} index={1}>

                    <Typography variant="span" component="span"><strong>Ahorro: </strong>El ahorro es la parte de los
                        ingresos
                        que la persona no gasta ni invierte. <samp>Fuente: Economipedia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Banco de la república:</strong> Es un organismo
                        del
                        Estado que desempeña las funciones de banco central. Las competencias especiales
                        asignadas
                        al Banco abarcan la de regular la moneda mediante la tasa de intervención y control de
                        la
                        inflación, los cambios internacionales y el crédito, emitir la moneda legal colombiana,
                        administrar las reservas internacionales, ser prestamista y banquero de los
                        establecimientos
                        de crédito y servir como agente fiscal del Gobierno. <samp>Fuente: Banco de la República
                            de
                            Colombia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Bolsa de Valores de Colombia (BVC):</strong> Es
                        una
                        entidad privada constituida como sociedad anónima, que se encarga de proveer la
                        infraestructura a los sistemas de negociación de Renta Variable, Renta Fija y Derivados
                        Estandarizados, es vigilada por la Superintendencia Financiera y también es un emisor
                        debido
                        a que sus acciones se cotizan en el mercado de valores.<samp> Fuente: BVC.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Bolsa de Valores: </strong>Es el escenario a
                        través
                        del
                        cual se negocian títulos o valores como las acciones.<samp> Fuente: Banco
                            Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Bursatil:</strong> Es un término que hace
                        referencia
                        a
                        todo lo relativo a la Bolsa de Valores, su actividad, operaciones y
                        negociaciones.<samp> Fuente: Banco Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Capital Social:</strong> LEs la contribución que
                        realizan los socios de una empresa y que forma parte del patrimonio de la empresa.
                        <samp>Fuente: Banco Davivienda</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Inflación:</strong> Es un indicador económico
                        que
                        evidencia el aumento continuo y generalizado de los precios de bienes y servicios en un
                        período de tiempo en un país. Se conoce también como el Índice de Precios al Consumidor.
                        <samp>Fuente: Banco Davivienda</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Inversión:</strong> Es la destinación de un
                        capital
                        buscando una ganancia o rentabilidad en el futuro que permita crecer ese capital.
                        <samp>Fuente: Banco Davivienda</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Mercados Desarrollados:</strong> Se denomina
                        mercado
                        desarrollado a cualquier país desarrollado en términos de economía y mercados de
                        capital.
                        <samp>Fuente: Economipedia</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Mercados Emergentes:</strong> Los mercados
                        emergentes
                        son aquellos países o economías que se encuentran en una fase de transición entre los
                        países
                        en vías de desarrollo y los países desarrollados.
                        <samp>Fuente: Economipedia</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>NASDAQ:</strong> Es el acrónimo de National
                        Association
                        of Securities Dealers Automated Quotation y es la segunda bolsa de valores electrónica
                        automatizada más grande de Estados Unidos. Se caracteriza por incluir las empresas de
                        alta
                        tecnología en electrónica, informática, telecomunicaciones, biotecnología, etc.
                        <samp>Fuente: Banco Santander.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Presupuesto:</strong> Un presupuesto es
                        documento
                        que
                        permite plasmar los ingresos y gastos de una persona con el fin de llevar unas finanzas
                        personales organizadas. La mejor manera de hacerlo es identificar en qué se está
                        gastando el
                        dinero ahora, de dónde proviene y a dónde se va, y para ello se deben anotar todos los
                        ingresos y gastos que se tengan antes de iniciar el presupuesto.
                        <samp>Fuente: Banco Scotiabank Colpatria.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Rentabilidad:</strong> Son las ganancias o
                        beneficios
                        que se obtienen al realizar una inversión en un período de tiempo.
                        <samp>Fuente: Banco Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Riesgo de una inversión:</strong> Es la posible
                        perdida
                        en la que se puede incurrir por tener una inversión.
                        <samp>Fuente: Banco Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Superintendencia Financiera:</strong> Es el
                        organismo
                        que se encarga de ejecutar la inspección, vigilancia y control sobre las personas que
                        realicen actividades financiera, bursátil, aseguradora y cualquier otra relacionada con
                        el
                        manejo, aprovechamiento o inversión de recursos captados del público.
                        <samp>Fuente: SIF.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Tasa de interés:</strong> Es un porcentaje
                        calculado
                        sobre un monto determinado que representa el costo que asume una persona o una compañía
                        por
                        acceder a recursos o capital. Por ello, se considera que la tasa de interés representa
                        el
                        costo del dinero en los mercados financieros.
                        <samp>Fuente: Banco Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Tasa de intervención:</strong> Es la tasa de
                        interés
                        mínima que cobra el Banco de la República a las entidades financieras por los préstamos
                        que
                        les hace, o la tasa de interés máxima que paga por recibirles dinero sobrante.
                        <samp>Fuente:Banco de la República de Colombia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Títulos de Renta Fija:</strong> Es la tasa de
                        interés
                        mínima que cobra el Banco de la República a las entidades financieras por los préstamos
                        que
                        les hace, o la tasa de interés máxima que paga por recibirles dinero sobrante.
                        <samp>Fuente:Banco de la República de Colombia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Títulos de Renta Variable:</strong> Son aquellos
                        títulos
                        donde la rentabilidad depende del comportamiento de los precios de esos instrumentos en
                        los
                        mercados financieros. Generalmente no tienen plazo de vencimiento.
                        <samp>Fuente: Banco Davivienda.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Valor presente:</strong> El valor presente es el
                        valor
                        que tiene en la actualidad una cantidad de dinero que no recibiremos ahora mismo sino
                        más
                        adelante, en el futuro.
                        <samp>Fuente: Banco BBVA.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Volatilidad:</strong>Es la variación promedio
                        que
                        podría
                        tener un activo frente a su precio esperado o frente al mercado. Se asocia al riesgo de
                        mercado entendiendo que a mayor volatilidad mayor el riesgo de mercado del activo.
                        <samp>Fuente: Banco Davivienda.</samp>
                    </Typography>
                </TabPanel>
                <TabPanel value={this.state.value} index={2}>
                    <Typography variant="span" component="span"><strong>Colcap: </strong>Es un índice de capitalización
                        que
                        refleja las variaciones de los precios de las acciones más líquidas de la Bolsa de
                        Valores
                        de Colombia (BVC), donde la participación de cada acción en el índice está determinada
                        por
                        el correspondiente valor de la capitalización bursátil ajustada (flotante de la compañía
                        multiplicado por el último precio). La canasta del índice COLCAP estará compuesta por
                        mínimo
                        20 acciones de 20 emisores diferentes. <samp>Fuente: Banco de la República de
                            Colombia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Down Jones:</strong> El índice Dow Jones es un
                        índice
                        bursátil constituido por las 30 compañías con mayor capitalización bursátil de la Bolsa
                        de
                        valores de New York (NYSE), con excepción de transporte y servicios públicos. <samp>Fuente:
                            Banco Scotiabank
                            Euroinnova.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Fear and Greed:</strong> El índice Fear & Greed
                        es
                        una
                        manera de medir los movimientos del mercado de valores y si las acciones tienen un
                        precio
                        justo. La lógica de este índice indica que el miedo excesivo tiende a hacer bajar los
                        precios de las acciones, y demasiada codicia tiende a tener el efecto
                        contrario.<samp> Fuente: CNN.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>S&P 500 Bond Index: </strong>Busca ser el
                        equivalente de
                        deuda corporativa del S&P 500. Este índice, ponderado por el valor de mercado, pretende
                        medir el desempeño de la deuda emitida por empresas estadounidenses que forman parte del
                        S&P
                        500.<samp> Fuente: S&P Global</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>S&P 500:</strong> El Standard & Poor's 500, es
                        uno
                        de
                        los índices bursátiles más importantes de Estados Unidos. El índice se basa en la
                        capitalización bursátil de 500 grandes empresas que poseen acciones que cotizan en las
                        bolsas NYSE o NASDAQ, y captura aproximadamente el 80% de toda la capitalización de
                        mercado
                        en Estados Unidos.<samp> Fuente: Investopedia</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Tasa de Cambio Representativa del Mercado
                        (TRM):</strong> Es la tasa de cambio oficial de pesos colombianos por un dólar
                        estadounidense calculada diariamente.
                        <samp>Fuente: Banco de la República de Colombia.</samp>
                    </Typography>
                    <br/><br/>
                    <Typography variant="span" component="span"><strong>Tasa de Captación:</strong> Las tasas de
                        captación
                        son
                        las tasas de interés que las instituciones financieras reconocen a los depositantes por
                        la
                        captación de sus recursos.
                        <samp>Fuente: Banco de la República de Colombia.</samp>
                    </Typography>
                </TabPanel>
            </div>
        );
    }
}

export default Glossary;
