export default class InstrumentType {
    _allRecommendations;
    _investments;
    _result = [];

    constructor(allRecommendations, investments) {
        this._allRecommendations = allRecommendations;
        this._investments = investments;
        this._allRecommendations = this._allRecommendations.filter((recommendation) => {
            return recommendation.tipo_validacion === 'instrumento';
        })
    }

    getRecommendations() {
        return this._validateQuantity();
    }

    _validateQuantity() {
        let investmentActive = this._investments.filter((investment) => {
            return investment.status;
        });
        return this._allRecommendations.filter((recommendation) => {
            return recommendation.valor_validacion == investmentActive.length;
        });
    }
}