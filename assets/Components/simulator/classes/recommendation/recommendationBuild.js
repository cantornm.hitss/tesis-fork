import InstrumentType from "./instrumentType";
import TimeType from "./timeType";
import InversionType from "./inversionType";
import ProfitsType from "./profitsType";

export default class RecommendationBuild {
    _recommendations;
    _result = [];
    _time;
    _changeNews;

    constructor(recommendations, investment, time, userGame, changeNews, totalInvestment, availableBalance) {
        this._recommendations = recommendations;
        this._time = time;
        this._changeNews = changeNews;
        this._instrumentType = new InstrumentType(recommendations, investment);
        this._timeType = new TimeType(recommendations, investment, time);
        this._inversionType = new InversionType(recommendations, investment, userGame);
        this._profitsType = new ProfitsType(recommendations, investment, changeNews, totalInvestment, availableBalance);
    }

    initCalculate() {
        let result = [];
        result = result.concat(this._instrumentType.getRecommendations());
        result = result.concat(this._timeType.getRecommendations());
        result = result.concat(this._inversionType.getRecommendations());

        if (this._changeNews) {
            result = result.concat(this._profitsType.getRecommendations());
        }
        return result;
    }
}