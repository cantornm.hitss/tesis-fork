export default class InversionType {
    _allRecommendations = [];
    _recommendations;
    _investments;
    _gameInfo;

    constructor(recommendations, investments, gameInfo) {
        this._recommendations = recommendations;
        this._investments = investments;
        this._gameInfo = gameInfo;

        this._allRecommendations = this._recommendations.filter((recommendation) => {
            return recommendation.tipo_validacion == 'inversion';
        });
    }

    getRecommendations() {
        return this._validateInvestment();
    }

    _getProfile(value) {
        switch (value) {
            case 'b':
                return 'conservador';
                break;
            case 'y':
                return 'moderado';
                break;
            default:
                return 'arriesgado';
                break;
        }
    }

    _validateInvestment() {
        let result = this._allRecommendations.filter((recommendation) => {
            let investment = [];
            switch (recommendation.paridad) {
                case "!=":
                    investment = this._investments.filter((investment) => {
                        return (!investment.name.includes(recommendation.valor_validacion) && this._getProfile(this._gameInfo.profile) == recommendation.perfil)
                    });

                    return investment.length > 0;
                    break;
                case "||":
                    let validationValues = recommendation.valor_validacion.split(";");

                    investment = this._investments.filter((investment) => {
                        return (investment.name.includes(validationValues[0]) || investment.name.includes(validationValues[1]) &&
                            this._getProfile(this._gameInfo.profile) == recommendation.perfil);
                    });

                    return investment.length > 0;
                    break;
            }
        });
        return result;
    }
}