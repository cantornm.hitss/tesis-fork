class Instruments {

    constructor() {
        const ETFFIJO = 3776.21;
        this.instrumentsArray = [
            new TypeInstruments("Bancolombia", "Acciones - Mercados Emergentes", 39180, 39180, 'Es el mayor banco/institución financiera de Colombia en términos de activos y patrimonio. Proporcionando servicios bancarios para personas y empresas.'),
            new TypeInstruments("Argos", "Acciones - Mercados Emergentes", 13350, 13350, 'Empresa productora y comercializadora de cemento y concreto, multinacional con presencia en 16 países'),
            new TypeInstruments("Nutresa", "Acciones - Mercados Emergentes", 44220, 44220, 'Empresa líder en participación de mercado de alimentos procesados en Colombia, con relevancia en América Latina y presencia directa en 14 países.'),
            new TypeInstruments("Grupo Sura", "Acciones - Mercados Emergentes", 31950, 31950, 'Grupo de Inversiones Suramericana, gestor de inversiones centrado en servicios financieros, industria y venture corporativo'),
            new TypeInstruments("Grupo Éxito", "Acciones - Mercados Emergentes", 17350, 17350, 'Empresa multinacional y cadena de supermercados más grande de Colombia, con presencia en Uruguay y Argentina.'),

            new TypeInstruments("Alphabet", "Acciones - Mercados Desarrollados", 9854057.76, 9854057.76, 'Conjunto de empresas estadounidenses de carácter multinacional de operación mundial, desarrolladora de productos y servicios relacionados con el internet, software, telecomunicaciones y otras tecnologías, cuyo pilar o filial principal es Google.'),
            new TypeInstruments("JP Morgan", "Acciones - Mercados Desarrollados", 566204.9274, 566204.9274, 'Empresa multinacional estadounidense con presencia en más de 100 países, dedicada a la banca de inversión y servicios financieros para consumidores, corporaciones, gobiernos e instituciones.'),
            new TypeInstruments("Meta", "Acciones - Mercados Desarrollados", 708454.7581, 708454.7581, 'Grupo de empresas estadounidense de carácter mundial (salvo para los países bloqueados) que opera en el sector de las TIC, con tecnología y redes sociales.'),
            new TypeInstruments("Amazon", "Acciones - Mercados Desarrollados", 10990621.4429, 10990621.4429, 'Empresa estadounidense de comercio exclusivamente electrónico y servicios de cloud computing a diferentes niveles con un catálogo diversificado de productos con presencia en múltiples países del mundo.'),
            new TypeInstruments("Apple", "Acciones - Mercados Desarrollados", 584292.9733, 584292.9733, 'Empresa multinacional estadounidense dedicada al diseño y comercialización de productos electrónicos, software y servicios on-line.'),

            new TypeInstruments("Bitcoin", "Criptomonedas", 148772478.233, 148772478.233, 'Moneda virtual o digital libre y descentralizada que funciona como medio de cambio electrónico para adquirir productos y servicios en transacciones directas sin intermediarios.'),

            new TypeInstruments("YPF", "Bonos", 278907.122, 278907.122, 'Empresa Argentina dedicada a la exploración, explotación y destilación de gas, energía eléctrica, petróleo y sus derivados. Moodys le otorga una calificación Caa3. '),
            new TypeInstruments("Banco Du Brasil", "Bonos", 388005.5775, 388005.5775, 'Institución financiera brasilera con gran participación del gobierno. Moodys le otorga una calificación Ba2.'),
            new TypeInstruments("Pemex", "Bonos", 357544.85, 357544.85, 'Empresa estatal mexicana dedicada a la producción y transporte de petróleo y gas natural. Moodys le otorga una calificación Ba3.'),

            new TypeInstruments("QQQ", "ETF", (324.4 * ETFFIJO), (324.4 * ETFFIJO), ''),
            new TypeInstruments("VWO", "ETF", (43.86 * ETFFIJO), (43.86 * ETFFIJO), ''),
            new TypeInstruments("USO", "ETF", (76.4 * ETFFIJO), (76.4 * ETFFIJO), ''),

            new TypeInstruments("CDT 31-44 dias", "CDTs Corto", 1.75244171, 1.75244171, 'Título  de renta fija a corto plazo'),
            new TypeInstruments("CDT 46-59 dias", "CDTs Medio-Corto", 5.40989825, 5.40989825, 'Título  de renta fija a medio-corto plazo'),
            new TypeInstruments("CDT 61-89 dias", "CDTs Medio-Largo", 5.01669279, 5.01669279, 'Título  de renta fija a medio-largo plazo'),
            new TypeInstruments("CDT +360 dias", "CDTs Largo", 8.04582414, 8.04582414, 'Título de renta fija a largo plazo'),

            new TypeInstruments("Euro", "Divisas", 4163, 4163, 'Moneda utilizada en el continente europeo, regulada por el banco Central europeo y la comision Europea.'),
            new TypeInstruments("Dólar", "Divisas", 3816.01, 3816.01, 'Moneda oficial de los Estados Unidos de America, es la moneda mas utilizada para transacciones internacionales en el mundo.'),
        ];
    }

    getNameAllInvestment() {
        let result = [];
        this.instrumentsArray.map(instrument => {
            result.push([instrument.nameType]);
        });
        return result;
    }

    getAllInvestment() {
        return this.instrumentsArray;
    }

    getInstrument(name) {
        return this.instrumentsArray.filter(instrument => instrument.nameType === name);
    }
}

class TypeInstruments {
    constructor(nameType, type, referenceValue, currentValue, description) {
        this.nameType = nameType;
        this.type = (new InvestmentType()).getInvestment(type)[0];
        this.referenceValue = referenceValue;
        this.currentValue = currentValue;
        this.description = description;
    }
}

class InvestmentType {
    constructor() {
        this.investmentTypes = [
            {
                type: "Acciones - Mercados Emergentes",
                periodicity: "1",
                indicator: "COLCAP",
                matriz: "Colcap",
                instrument: "Colcap",
                instrumentMatriz: "Colcap",
                days: 0,
                expiration: 0,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'quantity',
                status: true
            },
            {
                type: "Acciones - Mercados Desarrollados",
                periodicity: "1",
                indicator: "S&P 500",
                matriz: "SP",
                instrument: "SP",
                instrumentMatriz: "SP_500",
                days: 0,
                expiration: 0,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'quantity',
                status: true
            },
            {
                type: "Finca Raiz",
                periodicity: "360",
                indicator: "IPC",
                matriz: "",
                days: 0,
                expiration: 0,
                impact: 0,
                random: 0,
                simulated: 0,
                status: true
            },
            {
                type: "Bonos",
                periodicity: "180",
                indicator: "S&P 500 INDEX",
                matriz: "SP_500_BOND_INDEX",
                instrument: "SP index",
                instrumentMatriz: "SP_500_BOND_INDEX",
                days: 0,
                expiration: 3600,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'quantity',
                status: true
            },
            {
                type: "CDTs Corto",
                periodicity: "30",
                indicator: "TCA",
                matriz: "Tasa_Captacion",
                instrument: "TCA 31",
                instrumentMatriz: "Tasa_Captacion",
                days: 0,
                expiration: 30,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'amount',
                status: true
            },
            {
                type: "CDTs Medio-Corto",
                periodicity: "45",
                indicator: "TCA",
                matriz: "Tasa_Captacion",
                instrument: "TCA 46",
                instrumentMatriz: "Tasa_Captacion",
                days: 0,
                expiration: 45,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'amount',
                status: true
            },
            {
                type: "CDTs Medio-Largo",
                periodicity: "60",
                indicator: "TCA",
                matriz: "Tasa_Captacion",
                instrument: "TCA 61",
                instrumentMatriz: "Tasa_Captacion",
                days: 0,
                expiration: 60,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'amount',
                status: true
            },
            {
                type: "CDTs Largo",
                periodicity: "360",
                indicator: "TCA",
                matriz: "Tasa_Captacion",
                instrument: "TCA 360",
                instrumentMatriz: "Tasa_Captacion",
                days: 0,
                expiration: 360,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'amount',
                status: true
            },
            {
                type: "Criptomonedas",
                periodicity: "0",
                indicator: "FG",
                matriz: "Fear_Greed",
                instrument: "Criptomoneda",
                instrumentMatriz: "Fear_Greed",
                days: 0,
                expiration: 0,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'quantity',
                status: true
            },
            {
                type: "ETF",
                periodicity: "0",
                indicator: "DJ",
                matriz: "Down_Jones",
                instrument: "DJ",
                instrumentMatriz: "Down_Jones",
                days: 0,
                expiration: 0,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'quantity',
                status: true
            },
            {
                type: "Divisas",
                periodicity: "0",
                indicator: "TRM",
                matriz: "TRM",
                instrument: "TRM",
                instrumentMatriz: "TRM",
                days: 0,
                expiration: 0,
                impact: 0,
                random: 0,
                simulated: 0,
                investment: 'amount',
                status: true
            },
        ]
    }

    getInvestment(type) {
        return this.investmentTypes.filter(investement => investement.type === type);
    }
}

export default Instruments