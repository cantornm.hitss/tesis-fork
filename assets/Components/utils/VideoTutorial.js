import React, {Component} from "react";
import Backdrop from '@mui/material/Backdrop';
import {Box, Fade, Modal, Typography} from "@mui/material";

class VideoTutorial extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openVideo: false
        }
        this.handleDialog = this.handleDialog.bind(this);
    }

    handleDialog(value) {
        this.setState({
            openVideo: value
        });
    }

    render() {
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: '70%',
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };
        return (
            <div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    /*aria-describedby="transition-modal-description"*/
                    open={this.state.openVideo}
                    onClose={() => this.handleDialog(false)}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={this.state.openVideo}>
                        <Box sx={style}>
                            <Typography id="spring-modal-title" variant="h6" component="h2">
                                Antes de empezar, esta es la forma de jugar..
                            </Typography>
                            <div className="embed-responsive embed-responsive-21by9">
                                <video src={this.props.tutorial} controls>
                                    <source src={this.props.tutorial} type="video/mp4"/>
                                </video>
                            </div>
                        </Box>
                    </Fade>
                </Modal>
            </div>
        );
    }
}

export default VideoTutorial;