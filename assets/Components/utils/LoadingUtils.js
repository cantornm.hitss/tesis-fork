import React, {Component} from 'react';
import {Box, CircularProgress, Fade} from "@mui/material";
class LoadingUtils extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="divLoading" hidden={!this.props.loadingStatus?true:false}>
                <Box sx={{ height: 140 }}>
                    <Fade
                        in={this.props.loadingStatus}
                        style={{
                            transitionDelay: this.props.loadingStatus ? '800ms' : '0ms',
                        }}
                        unmountOnExit
                    >
                        <CircularProgress />
                    </Fade>
                </Box>
            </div>
        )
    }
}

export default LoadingUtils;