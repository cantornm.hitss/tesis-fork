import React, {Component} from "react";
import axios from "axios";
import LoadingUtils from "../utils/LoadingUtils";
import {Card, CardActionArea, CardContent, CardMedia, Typography} from "@mui/material";
import {Link} from "react-router-dom";

class index extends Component {
    constructor() {
        super();
        this.state = {
            id_game: 0,
            balloonTotal: 0,
            balloonAvailable: 0,
            totalCars: 0,
            availableCars: 0,
            clickFast: 0,
            balloonsAverage: 0,
            carsAverage: 0,
            profileAverage: 0,
            profile: ""
        };
    }

    calculateAverage() {
        let balloonsAverage = this.state.balloonTotal / this.state.balloonAvailable;
        let carsAverage = this.state.clickFast / this.state.totalCars;
        let profileAverage = ((carsAverage + balloonsAverage) / 2).toFixed(2);
        let profile = (profileAverage < 0.33) ? "b" : (profileAverage > 0.66) ? "r" : "y";

        this.setState({
            balloonsAverage: balloonsAverage.toFixed(2),
            carsAverage: carsAverage.toFixed(2),
            profileAverage: profileAverage,
            profile
        });

        this.disabledVideo(profile);
    }

    disabledVideo(profile) {
        let id = '';
        if (profile == 'b') {
            id = 'video_blue';
        } else if (profile == 'y') {
            id = 'video_yellow';
        } else {
            id = 'video_red';
        }
        let videoElement = document.getElementById(id);
        videoElement.removeAttribute('disabled');
        videoElement.classList.remove('col-md-4');
        videoElement.classList.add('col-md-6');
        videoElement.querySelector('video').play();
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    saveProfile() {
        axios.post(this.props.images_info.configInfo.urlNewProfile, this.state)
            .then(result => {

            }).catch(error => {

        });
    }

    componentDidMount() {
        this.loading(true);
        axios
            .get(this.props.images_info.configInfo.urlGame, {async: false})
            .then(result => {
                if (result.status == 200 && result.data.error == 0) {
                    let games = result.data.games;
                    if (typeof games.Activo != 'undefined') {
                        let game = games.Activo[0];
                        this.setState({
                            id_game: game.id_game,
                            balloonTotal: game.balloonTotal,
                            balloonAvailable: game.balloonAvailable,
                            totalCars: game.totalCars,
                            availableCars: game.availableCars,
                            clickFast: game.clickFast
                        });
                        this.calculateAverage();
                        this.saveProfile();
                    }
                }
                this.loading(false);
            })
            .catch(error => {
                this.loading(false);
            });
    }

    render() {
        return (
            <div className="container-fluid gtco-numbers-block block-video-presentacion">
                <LoadingUtils loadingStatus={this.state.loading}/>
                <div className="row mb-lg-5 justify-content-center">
                    <div className="col-md-3 mb-lg-5 pb-lg-5 pt-md-5 classvideo" id={"video_blue"} disabled>
                        <div className="embed-responsive embed-responsive-21by9">
                            <video src={this.props.images_info.itemVideos.presentacion_blue} controls>
                                <source src={this.props.images_info.itemVideos.presentacion_blue} type="video/mp4"/>
                            </video>
                        </div>
                    </div>
                    <div className="col-md-3 mb-lg-5 pb-lg-5 pt-md-5 classvideo" id={"video_yellow"} disabled>
                        <div className="embed-responsive embed-responsive-21by9">
                            <video src={this.props.images_info.itemVideos.presentacion_yellow} controls>
                                <source src={this.props.images_info.itemVideos.presentacion_yellow} type="video/mp4"/>
                            </video>
                        </div>
                    </div>
                    <div className="col-md-3 mb-lg-5 pb-lg-5 pt-md-5 classvideo" id={"video_red"} disabled>
                        <div className="embed-responsive embed-responsive-21by9">
                            <video src={this.props.images_info.itemVideos.presentacion_red} controls>
                                <source src={this.props.images_info.itemVideos.presentacion_red} type="video/mp4"/>
                            </video>
                        </div>
                    </div>
                    <div className="row mb-lg-5">
                        <div className="col-md-8 col-md-offset-2">
                            <div className="card">
                                <div className="card-body">
                                    <Link to="/game/simulator" className={"btn btn-primary btn-lg"}>Siguiente</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="container">
                        <svg width="100%" viewBox="0 0 1600 400">
                            <defs>
                                <linearGradient id="PSgrad_03" x1="80.279%" x2="0%" y2="0%">
                                    <stop offset="0%" stopColor="rgb(1,230,248)" stopOpacity="1"/>
                                    <stop offset="100%" stopColor="rgb(29,62,222)" stopOpacity="1"/>

                                </linearGradient>
                            </defs>
                            <path fillRule="evenodd" fill="url(#PSgrad_02)" opacity="0.102"
                                  d="M801.878,3.146 L116.381,128.537 C26.052,145.060 -21.235,229.535 9.856,312.073 L159.806,710.157 C184.515,775.753 264.901,810.334 338.020,792.380 L907.021,652.668 C972.912,636.489 1019.383,573.766 1011.301,510.470 L962.013,124.412 C951.950,45.594 881.254,-11.373 801.878,3.146 Z"/>

                            <clipPath id="ctm" fill="none">
                                <path
                                    d="M98.891,386.002 L1527.942,380.805 C1581.806,380.610 1599.093,335.367 1570.005,284.353 L1480.254,126.948 C1458.704,89.153 1408.314,59.820 1366.025,57.550 L298.504,0.261 C238.784,-2.944 166.619,25.419 138.312,70.265 L16.944,262.546 C-24.214,327.750 12.103,386.317 98.891,386.002 Z"/>
                            </clipPath>

                            <image clipPath="url(#ctm)" xlinkHref={this.props.images_info.itemImages.world}
                                   height="800px" width="100%" className="svg__image">

                            </image>

                        </svg>
                        <div className="row">
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">{this.state.balloonsAverage}</h5>
                                        <p className="card-text">Promedio Globos</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">{this.state.carsAverage}</h5>
                                        <p className="card-text">Promedio Carros</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">{this.state.profileAverage}</h5>
                                        <p className="card-text">Promedio Perfil</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default index;